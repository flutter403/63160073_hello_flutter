import 'package:flutter/material.dart';

void main() {
  runApp(HelloFlutterApp());
}

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String japaneseGreeting = "こんにちはフラッター";
String koreanGreeting = "헬로 플러터";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != spanishGreeting
                        ? spanishGreeting
                        : englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != japaneseGreeting
                        ? japaneseGreeting
                        : englishGreeting;
                  });
                },
                icon: Icon(Icons.translate)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != koreanGreeting
                        ? koreanGreeting
                        : englishGreeting;
                  });
                },
                icon: Icon(Icons.g_translate))
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(
                fontSize: 36, color: Colors.green, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}

// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 48, color: Colors.green),
//           ),
//         ),
//       ),
//     );
//   }
// }
